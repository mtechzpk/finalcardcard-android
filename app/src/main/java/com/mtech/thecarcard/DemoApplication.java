package com.mtech.thecarcard;

import android.content.Context;
import android.util.Log;

import com.telr.mobile.sdk.TelrApplication;

public class DemoApplication extends TelrApplication {

    private static Context context;

    public void onCreate(){
        super.onCreate();
        Log.d("Demo","Context Started....");
        DemoApplication.context = getApplicationContext();
    }

    public static Context getContext(){ return DemoApplication.context; }
}
