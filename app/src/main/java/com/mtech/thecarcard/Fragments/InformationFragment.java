package com.mtech.thecarcard.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mtech.thecarcard.Adapters.InformationAdapter;
import com.mtech.thecarcard.Models.InformationModel;
import com.mtech.thecarcard.R;

import java.util.ArrayList;

public class InformationFragment extends Fragment {


    private RecyclerView Information_Recyclerview;
    private InformationAdapter adapter;
    private ArrayList<InformationModel> infolist;
    LinearLayoutManager manager;


    String Question [] = {"Give Us A Call Or Email If You Have Some Questions!","Do You Have Any Question? Click Here To Call Us!",
    "Do You Have Any Question? Click Here To Mail Us!"};
    String Titile [] = {"contact information","call us","mail us"};
    Integer Images [] = {R.drawable.contact,R.drawable.contact_ic,R.drawable.mail_ic};

    public InformationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View  v =  inflater.inflate(R.layout.fragment_information, container, false);


        Information_Recyclerview = v.findViewById(R.id.infomartion_recyclerview);
        manager = new LinearLayoutManager(getContext());
        infolist = new ArrayList<>();

        Information_Recyclerview.setHasFixedSize(true);
        Information_Recyclerview.setLayoutManager(manager);

        for (int i =0; i<=2;  i++){

            InformationModel item = new InformationModel();
            item.setImage(Images[i]);
            item.setTitle(Titile[i]);
            item.setQuestion(Question[i]);
            infolist.add(item);
        }

        adapter = new InformationAdapter(getContext(),infolist);
        Information_Recyclerview.setAdapter(adapter);
        return v;
    }
}