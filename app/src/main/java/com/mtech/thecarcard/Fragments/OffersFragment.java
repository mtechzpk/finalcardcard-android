package com.mtech.thecarcard.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mtech.thecarcard.Adapters.OffersAdapter;
import com.mtech.thecarcard.Models.OfferDataModel;
import com.mtech.thecarcard.Models.OfferResponseModel;
import com.mtech.thecarcard.Models.OffersModel;
import com.mtech.thecarcard.Network.GetDataService;
import com.mtech.thecarcard.Network.RetrofitClientInstance;
import com.mtech.thecarcard.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class OffersFragment extends Fragment {


    private RecyclerView recyclerView;
    private OffersAdapter adapter;
    private ArrayList<OffersModel> list;
    LinearLayoutManager manager;

    ProgressDialog progressDialog;

    public OffersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View  v = inflater.inflate(R.layout.fragment_offers, container, false);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<OfferResponseModel> call = service.getAllOffers();

        call.enqueue(new Callback<OfferResponseModel>() {
            @Override
            public void onResponse(Call<OfferResponseModel> call, Response<OfferResponseModel> response) {
                progressDialog.dismiss();
                generateOfferList(response.body().getData());
            }

            @Override
            public void onFailure(Call<OfferResponseModel> call, Throwable t) {
                t.printStackTrace();

            }
        });

        recyclerView = v.findViewById(R.id.offers_recyclerview);
//        manager = new LinearLayoutManager(getContext());
//        list = new ArrayList<>();
//
//        Offers_Recyclerview.setHasFixedSize(true);
//        Offers_Recyclerview.setLayoutManager(manager);
//
//        for (int i =0; i<=5;  i++){
//
//            OffersModel item = new OffersModel();
//            item.setImage(R.drawable.sunstore);
//            item.setStore("Sunstore - free shipping");
//            item.setDate("2020-10-17");
//            item.setOffers("Free Shipping Whole Summer For All Members!");
//            list.add(item);
//     }

//        adapter = new OffersAdapter(getContext(),list);
//        Offers_Recyclerview.setAdapter(adapter);


        return  v;
    }

    private void generateOfferList(List<OfferDataModel> data) {

        adapter = new OffersAdapter(getContext(),data);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }
}