package com.mtech.thecarcard.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mtech.thecarcard.Adapters.NewsAdapter;
import com.mtech.thecarcard.Models.NewsModel;
import com.mtech.thecarcard.Models.NewsResponseModel;
import com.mtech.thecarcard.Network.GetDataService;
import com.mtech.thecarcard.Network.RetrofitClientInstance;
import com.mtech.thecarcard.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsFragment extends Fragment {



    private RecyclerView News_Recyclerview;
    private NewsAdapter adapter;
    LinearLayoutManager manager;
    Activity activity;
    ProgressDialog progressDialog;

    public NewsFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_news, container, false);

        activity = getActivity();
        News_Recyclerview = v.findViewById(R.id.news_recyclerview);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<NewsResponseModel> call = service.getAllNews();

        call.enqueue(new Callback<NewsResponseModel>() {
            @Override
            public void onResponse(Call<NewsResponseModel> call, Response<NewsResponseModel> response) {

                progressDialog.dismiss();
                assert response.body() != null;
                int status = 400;
                if ( response.body() != null){
                    status = response.body().getStatus();
                }
                if (!String.valueOf(status).isEmpty() && status == 200){
                    List dataList = response.body().getData();
                    if (dataList == null && activity != null){
                        Toast.makeText(activity, "No Record Found", Toast.LENGTH_LONG).show();
                    }
                    else {
                        getNews(News_Recyclerview, dataList);
                    }

                }
                else if (!String.valueOf(status).isEmpty() && status == 400 && activity != null){
                    Toast.makeText(activity, response.body().getMessage()  + "Failed", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<NewsResponseModel> call, Throwable t) {

                t.printStackTrace();
            }
        });



//
        return  v;
    }

    private void getNews(RecyclerView news_recyclerview, List dataList) {
        adapter = new NewsAdapter(getContext(),dataList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        News_Recyclerview.setLayoutManager(layoutManager);
        News_Recyclerview.setAdapter(adapter);
    }


}