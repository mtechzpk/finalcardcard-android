package com.mtech.thecarcard.dialogs;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mtech.thecarcard.Activities.MemberShipDetailsActivity;
import com.mtech.thecarcard.R;
import com.mtech.thecarcard.Server.MySingleton;
import com.mtech.thecarcard.Server.Server;
import com.mtech.thecarcard.utilities.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class EditProfileDialog extends DialogFragment implements
        AdapterView.OnItemSelectedListener {
    ImageView ivClose;
    Button btn_update;
    TextView up_dateOfBirth;
    EditText edit_address, edit_phone, state, etCountry;
    String getType;
    Spinner spinner;
    String getdate;
    int id;
    private DatePickerDialog.OnDateSetListener mDateSetListener;

    public static EditProfileDialog newInstance(String title) {
        EditProfileDialog frag = new EditProfileDialog();
        Bundle args = new Bundle();
        args.putInt("title", Integer.parseInt(title));
        frag.setArguments(args);
        return frag;
    }
//    Select Type

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.custom_profile, container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        id=Utilities.getInt(getContext(), "userId");
        String name=Utilities.getString(getActivity(),"user_name");
        String address=Utilities.getString(getActivity(),"user_address");
        String phonee=Utilities.getString(getActivity(),"user_phone");
        String dob=Utilities.getString(getActivity(),"dob");
        up_dateOfBirth = view.findViewById(R.id.up_dateOfBirth);
        edit_address = view.findViewById(R.id.edit_address);
        edit_phone = view.findViewById(R.id.edit_phone);
        btn_update = view.findViewById(R.id.btn_update);
        edit_address.setText(address);
        up_dateOfBirth.setText(dob);
        edit_phone.setText(phonee);
        getdate=dob;
        getDialog().setCancelable(false);
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        up_dateOfBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        getActivity(),
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
//                Log.d(TAG, "onDateSet: mm/dd/yyy: " + month + "/" + day + "/" + year);
                getdate = day + "-" + month + "-" + year;
                up_dateOfBirth.setText(getdate);
            }
        };

        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String getPhone = edit_phone.getText().toString();
                String getaddress = edit_address.getText().toString();
                if (!getPhone.equals("")) {
                    if (!getaddress.equals("")) {
                            editProfile(getPhone,getaddress);
                    } else {

                        Toast.makeText(getActivity(), "Enter Address", Toast.LENGTH_SHORT).show();
                    }
                } else {

                    Toast.makeText(getActivity(), "Enter Phone", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.MyAnimation_Window;
    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
        getType = (String) arg0.getItemAtPosition(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    private void editProfile(final String phone, final String addressss) {

        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please Wait");


        final StringRequest qmeRequest = new StringRequest(Request.Method.POST, Server.update_profile, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    String message = object.getString("message");
                    if (status == 200) {
                        JSONObject object1=object.getJSONObject("data");
                        int id=object1.getInt("id");
                        String address=object1.getString("address");
                        String email=object1.getString("email");
                        String phone=object1.getString("phone");
                        String dateofbirth=object1.getString("dateofbirth");
                        Utilities.saveString(getActivity(),"user_address",address);
                        Utilities.saveString(getActivity(),"user_phone",phone);
                        Utilities.saveString(getActivity(),"dob",dateofbirth);
                        progressDialog.dismiss();
                        dismiss();
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", String.valueOf(id));
                params.put("address", addressss);
                params.put("phone", phone);
                params.put("dateofbirth", getdate);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                params.put("Accept", "application/json");
                return params;
            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(qmeRequest);
    }
}