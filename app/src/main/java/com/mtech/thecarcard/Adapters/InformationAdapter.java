package com.mtech.thecarcard.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mtech.thecarcard.Models.InformationModel;
import com.mtech.thecarcard.R;

import java.util.ArrayList;

public class InformationAdapter extends RecyclerView.Adapter<InformationAdapter.ViewHolder> {

    private Context context;
    private ArrayList<InformationModel> list;

    public InformationAdapter(Context context, ArrayList<InformationModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.single_information_layout,parent,false);
        return  new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        InformationModel model = list.get(position);
        holder.iv_Image.setImageResource(model.getImage());
        holder.tv_Title.setText(model.getTitle());
        holder.tv_Question.setText(model.getQuestion());



    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public  class  ViewHolder extends RecyclerView.ViewHolder{

        private ImageView iv_Image;
        private TextView tv_Title,tv_Question;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_Image = itemView.findViewById(R.id.iv_image);
            tv_Title = itemView.findViewById(R.id.tv_title);
            tv_Question = itemView.findViewById(R.id.tv_time);

        }
    }

}
