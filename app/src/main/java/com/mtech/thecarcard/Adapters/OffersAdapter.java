package com.mtech.thecarcard.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mtech.thecarcard.Activities.OfferActivity;
import com.mtech.thecarcard.Models.OfferDataModel;
import com.mtech.thecarcard.R;
import com.mtech.thecarcard.utilities.Utilities;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class OffersAdapter extends RecyclerView.Adapter<OffersAdapter.ViewHolder> {

    private Context context;
    private List<OfferDataModel> list;
    DateFormat theDateFormat;
    Date date;
    public OffersAdapter(Context context, List<OfferDataModel> list) {
        this.context = context;
        this.list = list;
    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.single_offers_layout,parent,false);
        return  new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        OfferDataModel model = list.get(position);
String getServerdate=model.getCreated_at();
        holder.tv_Store.setText(model.getPost_title());
//        holder.tv_Date.setText(model.getCreated_at());
        holder.tv_Offer.setText(model.getDescription());
         theDateFormat = new SimpleDateFormat("yyyy-MM-dd");
         date = null;

        try {
            date = theDateFormat.parse(getServerdate);
        } catch (ParseException parseException) {
// Date is invalid. Do what you want.
        } catch (Exception exception) {
// Generic catch. Do what you want.
        }

        theDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String[] arrayString = getServerdate.split(";");

        String time = arrayString[0];

        time = time.substring(time.indexOf("2020-10-12T") + 12, time.length());
        DateFormat f1 = new SimpleDateFormat("HH:mm:ss"); //HH for hour of the day (0 - 23)
        Date d = null;
        try {
            d = f1.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat f2 = new SimpleDateFormat("h:mm a");
        String timeFormate=f2.format(d).toLowerCase(); // "12:18am"
         holder.tv_Date.setText(theDateFormat.format(date));

        Utilities.saveString(context,"date",timeFormate);
        Glide.with(holder.itemView)
                .load(model.getImage())
                .centerCrop()
                .into(holder.iv_Image);



        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, OfferActivity.class);
                intent.putExtra("date",theDateFormat.format(date));
                intent.putExtra("postName", model.getPost_title());
                intent.putExtra("postDetail", model.getDescription());
                intent.putExtra("image1", model.getImage());
                context.startActivity(intent);

            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public  class  ViewHolder extends RecyclerView.ViewHolder{

        private ImageView iv_Image;
        private TextView tv_Store,tv_Date,tv_Offer;
        private LinearLayout layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_Image = itemView.findViewById(R.id.iv_image);
            tv_Store = itemView.findViewById(R.id.tv_store);
            tv_Date = itemView.findViewById(R.id.tv_date);
            tv_Offer = itemView.findViewById(R.id.tv_offers);
            layout = itemView.findViewById(R.id.layout);
        }
    }

}
