package com.mtech.thecarcard.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.mtech.thecarcard.Activities.BuyMemberShipActivity2;
import com.mtech.thecarcard.Activities.MemberShipDetailsActivity;
import com.mtech.thecarcard.Activities.PayActivity;
import com.mtech.thecarcard.Models.PackageDataModel;
import com.mtech.thecarcard.R;
import com.mtech.thecarcard.utilities.Utilities;

import java.util.List;

public class ViewPagerAdapter extends PagerAdapter {

    public Context context;
    public List<PackageDataModel> list;

    public ViewPagerAdapter(Context context, List<PackageDataModel> list) {
        this.context = context;
        this.list = list;
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {

        return view == object;
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public Object instantiateItem(@NonNull final ViewGroup container, int position) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_viewpager, null);

        final Button select = view.findViewById(R.id.btn_select);
        final TextView packType = view.findViewById(R.id.packagetype);
        final TextView tv_One = view.findViewById(R.id.tv_one);
        final TextView tv_Two = view.findViewById(R.id.tv_two);
        final TextView tv_Three = view.findViewById(R.id.tv_three);

        PackageDataModel model =list.get(position);
        int packId = model.getId();
        tv_One.setText("The amount of this packaeg is"+" "+model.getAmount());
        tv_Two.setText("The Time duration of this package is"+" "+model.getDuration());
        tv_Three.setText(model.getDescription());
        String coupon_code=model.getCoupon_code();
        String discountAmount =model.getDiscount_amount();

        String coupan_number =BuyMemberShipActivity2.coupann;


        switch (position)
        {
            case 0:
                packType.setText("Package 1");
                break;
            case 1:
                packType.setText("Package 2");
                break;
            default:
                return null;
        }


//        select.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//
//
//                    Intent i = new Intent(context,PayActivity.class);
//                    Utilities.saveInt(context,"packageid",model.getId());
//                    context.startActivity(i);
//                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            }
//        });
        select.setOnClickListener(view1 -> {
            Intent mainIntent = new Intent(view.getContext(), MemberShipDetailsActivity.class);
            mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            Utilities.saveInt(context,"packageId",packId);
            Utilities.saveString(context,"amount",model.getAmount());
            Utilities.saveString(context,"coupon_code",coupon_code);
            Utilities.saveString(context,"discountAmount",discountAmount);
            Utilities.saveString(context,"coupan_number",coupan_number);

            view.getContext().startActivity(mainIntent);
        });

        container.addView(view, 0);

        return view;
    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {

        container.removeView((View) object);
    }
}
