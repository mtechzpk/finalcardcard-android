package com.mtech.thecarcard.Adapters;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.mtech.thecarcard.Fragments.InformationFragment;
import com.mtech.thecarcard.Fragments.NewsFragment;
import com.mtech.thecarcard.Fragments.OffersFragment;

public class HomeTabsAdapter extends FragmentStatePagerAdapter {

    public HomeTabsAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new OffersFragment();
            case 1:
                return new NewsFragment();
            case 2 :
                return new InformationFragment();

        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Offers";
            case 1:
                return "News";
            case 2:
                return "Information";

            default:
                return null;
        }
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}
