package com.mtech.thecarcard.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mtech.thecarcard.Models.NewsDataModel;

import com.mtech.thecarcard.Models.OfferDataModel;
import com.mtech.thecarcard.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {

    private Context context;
    private List<NewsDataModel> list;
    DateFormat theDateFormat;
    Date date;
    public NewsAdapter(Context context,List<NewsDataModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.single_news_layout,parent,false);
        return  new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        NewsDataModel model = list.get(position);
        String getServerdate=model.getCreated_at();
        theDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        date = null;

        try {
            date = theDateFormat.parse(getServerdate);
        } catch (ParseException parseException) {
// Date is invalid. Do what you want.
        } catch (Exception exception) {
// Generic catch. Do what you want.
        }

        theDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String[] arrayString = getServerdate.split(";");

        String time = arrayString[0];

        time = time.substring(time.indexOf("2020-10-12T") + 12, time.length());
        DateFormat f1 = new SimpleDateFormat("HH:mm:ss"); //HH for hour of the day (0 - 23)
        Date d = null;
        try {
            d = f1.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat f2 = new SimpleDateFormat("h:mm a");
        String timeFormate=f2.format(d).toLowerCase(); // "12:18am"
        holder.tv_Time.setText(theDateFormat.format(date));

        Glide.with(holder.itemView)
                .load(model.getImage())
                .centerCrop()
                .into(holder.iv_Image);

        holder.tv_Title.setText(model.getTitle());
        holder.tv_Announcement.setText(model.getSub_title());


    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public  class  ViewHolder extends RecyclerView.ViewHolder{

        private ImageView iv_Image;
        private TextView tv_Title,tv_Time,tv_Announcement;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_Image = itemView.findViewById(R.id.iv_image);
            tv_Title = itemView.findViewById(R.id.tv_title);
            tv_Time = itemView.findViewById(R.id.tv_time);
            tv_Announcement = itemView.findViewById(R.id.tv_announcement);
        }
    }

}
