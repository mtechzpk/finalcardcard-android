package com.mtech.thecarcard.Network;

import com.mtech.thecarcard.Models.LoginResponse;
import com.mtech.thecarcard.Models.NewsResponseModel;
import com.mtech.thecarcard.Models.OfferResponseModel;
import com.mtech.thecarcard.Models.PackageResponseModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface GetDataService {




    @FormUrlEncoded
    @POST("signup")
    Call<LoginResponse> SignUpUser(@Field("name") String name,
                                   @Field("phone") String phone,
                                   @Field("email") String email,
                                   @Field("password") String password,
                                   @Field("vechle_name") String vechle_name,
                                   @Field("vechle_model") String vechle_model,
                                   @Field("vechle_year") String vechle_year,
                                   @Field("dateofbirth") String dateofbirth,
                                   @Field("address") String address,
                                   @Field("pakage_id") String pakage_id,
                                   @Field("coupon_code") String coupon_code

    );
    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse>  loginUser(@Field("email") String email,
                                   @Field("password") String password);


    @GET("list_posts")
    Call<OfferResponseModel> getAllOffers();

    @GET("list_news")
    Call<NewsResponseModel> getAllNews();


    @GET("get_pakage")
    Call<PackageResponseModel> getAllPackage();

}


