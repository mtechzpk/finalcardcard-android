package com.mtech.thecarcard.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mtech.thecarcard.R;
import com.mtech.thecarcard.dialogs.EditProfileDialog;
import com.mtech.thecarcard.utilities.Utilities;

public class SettingActivity extends AppCompatActivity {

    TextView profile,membership,logout;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);


        profile = findViewById(R.id.myProfile);
        back = findViewById(R.id.back_setting);

//        profile.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                AlertDialog.Builder builder = new AlertDialog.Builder(SettingActivity.this);
//                ViewGroup viewGroup = findViewById(android.R.id.content);
//                View viewDialog = LayoutInflater.from(view.getContext()).inflate(R.layout.custom_profile,viewGroup,false);
//                builder.setView(viewDialog);
//
//                final Button updateProfile = viewDialog.findViewById(R.id.btn_update);
//                updateProfile.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Toast.makeText(getApplicationContext(),"Profile Update",Toast.LENGTH_SHORT).show();
//                    }
//                });
//
//
//                AlertDialog dialog =builder.create();
//                dialog.show();
//
//
//
//            }
//        });
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = EditProfileDialog.newInstance(
                        String.valueOf(R.string.search_word_meaning_dialog_title));
                newFragment.show(getSupportFragmentManager(), "dialog");
            }
        });

        logout = findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logoutUser();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SettingActivity.this,HomeActivity.class));
                finish();
            }
        });
    }
    public void logoutUser(){
        Utilities.clearSharedPref(getApplicationContext());
        Intent mainIntent = new Intent(SettingActivity.this, LoginActivity.class);
        startActivity(mainIntent);
        finish();
    }
}