package com.mtech.thecarcard.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.mtech.thecarcard.Adapters.HomeTabsAdapter;
import com.mtech.thecarcard.R;
import com.google.android.material.tabs.TabLayout;
import com.mtech.thecarcard.utilities.Utilities;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class HomeActivity extends AppCompatActivity {


    private ImageView iv_Logout,iv_Setting;
    private EditText ed_Name,ed_MemberShipNumber,ed_ExpityDate;

    TabLayout tabLayout;
    ViewPager viewPagerTabs;
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
       int  id=Utilities.getInt(HomeActivity.this, "userId");
        iv_Logout = findViewById(R.id.logout_ic);
        iv_Setting = findViewById(R.id.setting_ic);
        ed_Name = findViewById(R.id.ed_name);
        ed_MemberShipNumber = findViewById(R.id.ed_membership_number);
        ed_ExpityDate = findViewById(R.id.expiry_date);
        tabLayout = findViewById(R.id.home_tabLayout);
        viewPagerTabs = findViewById(R.id.viewPagerHomeTabs);
        String name = Utilities.getString(getApplicationContext(),"name");
        String expiryDate = Utilities.getString(getApplicationContext(),"expireDate");
        String membershipToken = Utilities.getString(getApplicationContext(),"number");


        ed_Name.setText(name);
        ed_MemberShipNumber.setText(membershipToken);


        DateFormat theDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;

        try {
            date = theDateFormat.parse(expiryDate);
        } catch (ParseException parseException) {
// Date is invalid. Do what you want.
        } catch (Exception exception) {
// Generic catch. Do what you want.
        }

        theDateFormat = new SimpleDateFormat("MMM dd, yyyy");
        String[] arrayString = expiryDate.split(";");

        String time = arrayString[0];

        time = time.substring(time.indexOf("2020-10-12T") + 12, time.length());
// tvDate.setText(theDateFormat.format(date));
        DateFormat f1 = new SimpleDateFormat("HH:mm:ss"); //HH for hour of the day (0 - 23)
        Date d = null;
        try {
            d = f1.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat f2 = new SimpleDateFormat("h:mm a");
        String timeFormate =f2.format(d).toLowerCase(); // "12:18am"

        ed_ExpityDate.setText(theDateFormat.format(date));




        iv_Logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logoutUser();
            }
        });

        iv_Setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this,SettingActivity.class));
                finish();
            }
        });


        final HomeTabsAdapter myPagerAdapter = new HomeTabsAdapter(this.getSupportFragmentManager());
        viewPagerTabs.setAdapter(myPagerAdapter);
        tabLayout.setupWithViewPager(viewPagerTabs);
        setMarginBetweenTabs(tabLayout);




        viewPagerTabs.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {


            }
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            public void onPageSelected(int position) {
                switch (position) {
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void setMarginBetweenTabs(TabLayout tabLayout) {

        ViewGroup tabs = (ViewGroup) tabLayout.getChildAt(0);

        for (int i = 0; i < tabs.getChildCount() - 1; i++) {
            View tab = tabs.getChildAt(i);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
            layoutParams.setMarginEnd(15);
            tab.setLayoutParams(layoutParams);
            tabLayout.requestLayout();
        }

    }
    public void logoutUser(){
        Utilities.clearSharedPref(getApplicationContext());
        Intent logout = new Intent(HomeActivity.this, LoginActivity.class);
        startActivity(logout);
        finish();
    }
}