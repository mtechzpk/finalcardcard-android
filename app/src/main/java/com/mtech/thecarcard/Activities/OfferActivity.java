package com.mtech.thecarcard.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mtech.thecarcard.R;

public class OfferActivity extends AppCompatActivity {

    private TextView tv_StoreName,tv_Date,tv_Details;
    private Button btn_ToGoStore;
    private ImageView back_ic,imageViewOffer;

    String postTitle,postDetail,postDate,image1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer);

        imageViewOffer = findViewById(R.id.offerImage);
        tv_StoreName = findViewById(R.id.tv_storename);
        tv_Date = findViewById(R.id.tv_date);
        tv_Details = findViewById(R.id.tv_details);
        btn_ToGoStore = findViewById(R.id.btn_togostore);
        back_ic = findViewById(R.id.back_ic);
        init();


        back_ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });


        btn_ToGoStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Toast.makeText(OfferActivity.this, "Selected", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void init() {

        postTitle = getIntent().getStringExtra("postName");
        postDetail = getIntent().getStringExtra("postDetail");
        postDate = getIntent().getStringExtra("date");
        image1 = getIntent().getStringExtra("image1");


        tv_StoreName.setText(postTitle);
        tv_Details.setText(postDetail);
        tv_Date.setText(postDate);

        Glide.with(this)
                .load(image1).centerCrop().into(imageViewOffer);
    }
}