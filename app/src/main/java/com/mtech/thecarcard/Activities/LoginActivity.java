package com.mtech.thecarcard.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mtech.thecarcard.Network.GetDataService;
import com.mtech.thecarcard.Models.LoginResponse;
import com.mtech.thecarcard.Network.RetrofitClientInstance;
import com.mtech.thecarcard.R;
import com.mtech.thecarcard.Server.MySingleton;
import com.mtech.thecarcard.Server.Server;
import com.mtech.thecarcard.utilities.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    Button btn_BuyMemerShip, btn_Login;

    EditText ed_Email, ed_Password;
    final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btn_BuyMemerShip = findViewById(R.id.btn_buymemerShip);
        btn_Login = findViewById(R.id.btn_login);
        ed_Email = findViewById(R.id.edit_email);
        ed_Password = findViewById(R.id.edit_password);


        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Signing In...");


        btn_BuyMemerShip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(LoginActivity.this, BuyMemberShipActivity2.class));
            }
        });

        btn_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = ed_Email.getText().toString();
                String password = ed_Password.getText().toString();
                if (isValidEmail(email)) {
                if (!email.isEmpty()) {
                    if (!password.isEmpty()) {

                            loginApi(email, password);

                        } else {
                        Toast.makeText(LoginActivity.this, "Please Add the missing fields", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(LoginActivity.this, "Please Add the missing fields", Toast.LENGTH_LONG).show();

                    }
                } else {
                    ed_Email.setError("please enter correct email address");
                    Toast.makeText(LoginActivity.this, "Enter valid email address", Toast.LENGTH_LONG).show();


                }

            }
        });


    }

    //    private void sendLoginRequest(String email, String password) {
//
//        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
//
//        Call<LoginResponse> call = service.loginUser(email,password);
//
//        call.enqueue(new Callback<LoginResponse>() {
//            @Override
//            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
//
//                 progressDialog.dismiss();
//                assert response.body() != null;
//                int status = response.body().getStatus();
//
//                if (status == 200) {
//                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                    Intent homeIntent = new Intent(LoginActivity.this, HomeActivity.class);
//
//                    int userId = response.body().getData().getId();
//                    String address=response.body().getData().getName();
//                    String email=response.body().getData().getEmail();
//                    String phone=response.body().getData().getPhone();
//                    String dateofbirth=response.body().getData().getDateofbirth();
//                    com.mtech.thecarcard.utilities.Utilities.saveString(LoginActivity.this,"user_address",address);
//                    com.mtech.thecarcard.utilities.Utilities.saveString(LoginActivity.this,"user_phone",phone);
//                    com.mtech.thecarcard.utilities.Utilities.saveString(LoginActivity.this,"dob",dateofbirth);
//                    com.mtech.thecarcard.utilities.Utilities.saveString(LoginActivity.this,"user_email",email);
//               Utilities.saveInt(LoginActivity.this,"userId",userId);
//                    Utilities.saveString(getApplicationContext(),"login_status","yes");
//
//                    startActivity(homeIntent);
//                    finish();
//                }
//                else if (status == 400){
//                    Toast.makeText(getApplicationContext(), response.body().getMessage()  + "Failed", Toast.LENGTH_LONG).show();
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<LoginResponse> call, Throwable t) {
//                 progressDialog.dismiss();
//                Toast.makeText(getApplicationContext(), t.getMessage() + "Not Called", Toast.LENGTH_LONG).show();
//
//            }
//        });
//
//    }
    private void loginApi(String email, String password) {

        ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage("Please Wait");
progressDialog.show();
        final StringRequest qmeRequest = new StringRequest(Request.Method.POST, Server.login, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    String message = object.getString("message");
                    if (status == 200) {
                        progressDialog.dismiss();
                        JSONObject object1 = object.getJSONObject("data");
                        int id = object1.getInt("id");
                        String name = object1.getString("name");
                        String address = object1.getString("address");
                        String email = object1.getString("email");
                        String phone = object1.getString("phone");
                        String membership_token = object1.getString("membership_token");
                        String expire_date = object1.getString("expire_date");
                        String dateofbirth = object1.getString("dateofbirth");
                        Utilities.saveString(LoginActivity.this, "user_address", address);
                        Utilities.saveString(LoginActivity.this, "user_phone", phone);
                        Utilities.saveString(LoginActivity.this, "dob", dateofbirth);
                        Utilities.saveInt(LoginActivity.this, "userId", id);
                        Utilities.saveString(getApplicationContext(), "number", membership_token);
                        Utilities.saveString(getApplicationContext(), "name", name);
                        Utilities.saveString(getApplicationContext(), "expireDate", expire_date);
                        Utilities.saveString(getApplicationContext(), "login_status", "yes");
                        progressDialog.dismiss();
                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                        finish();
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (LoginActivity.this != null)
                    Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                params.put("Accept", "application/json");
                return params;
            }
        };

        qmeRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(LoginActivity.this).addToRequestQueue(qmeRequest);
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
}