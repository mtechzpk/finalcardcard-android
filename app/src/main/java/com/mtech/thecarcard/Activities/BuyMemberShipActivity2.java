package com.mtech.thecarcard.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mtech.thecarcard.Adapters.ViewPagerAdapter;
import com.mtech.thecarcard.Animation.ViewPagerAnimation;
import com.mtech.thecarcard.Models.NewsResponseModel;
import com.mtech.thecarcard.Models.PackageDataModel;
import com.mtech.thecarcard.Models.PackageResponseModel;
import com.mtech.thecarcard.Network.GetDataService;
import com.mtech.thecarcard.Network.RetrofitClientInstance;
import com.mtech.thecarcard.R;
import com.mtech.thecarcard.utilities.Utilities;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BuyMemberShipActivity2 extends AppCompatActivity {

    ViewPager viewPager;
    ViewPagerAdapter adpter;
    private ImageView back_ic;
    TextView textView;
    EditText editText_coupen;

public static  String coupann;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_member_ship2);

        back_ic = findViewById(R.id.back_ic);
        viewPager = findViewById(R.id.viewPager);
        textView = findViewById(R.id.packagetype);

        editText_coupen = findViewById(R.id.ed_coupen);

        String coupon = editText_coupen.getText().toString();
        coupann=coupon;
//         Utilities.saveString(getApplicationContext(),"coupon_number",coupon);

        back_ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view){

                finish();
            }
        });


        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<PackageResponseModel> call = service.getAllPackage();
        call.enqueue(new Callback<PackageResponseModel>() {
            @Override
            public void onResponse(Call<PackageResponseModel> call, Response<PackageResponseModel> response) {

                assert response.body() != null;
                int status = response.body().getStatus();
                if (status==200)
                {

                    List<PackageDataModel> dataModelList = response.body().getData();

                    getPackage(viewPager,dataModelList);
                }
                else {
                    Toast.makeText(BuyMemberShipActivity2.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
            @Override
            public void onFailure(Call<PackageResponseModel> call, Throwable t) {
                t.printStackTrace();
            }
        });


}
    private void getPackage(ViewPager viewPager, List<PackageDataModel> dataList) {

    adpter = new ViewPagerAdapter(getApplicationContext(),dataList);
        viewPager.setPadding(0, 0, 0, 0);
        viewPager.setCurrentItem(0);
        viewPager.setPageTransformer(true, new ViewPagerAnimation());
        viewPager.setAdapter(adpter);

    }
}