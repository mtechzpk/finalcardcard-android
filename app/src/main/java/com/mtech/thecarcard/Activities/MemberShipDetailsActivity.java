package com.mtech.thecarcard.Activities;

import androidx.appcompat.app.AppCompatActivity;


import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.mtech.thecarcard.Models.ModelVehicleModel;
import com.mtech.thecarcard.Models.VehicleTypeModel;
import com.mtech.thecarcard.Models.VehicleYearModel;
import com.mtech.thecarcard.Network.GetDataService;
import com.mtech.thecarcard.Models.LoginResponse;
import com.mtech.thecarcard.Network.RetrofitClientInstance;
import com.mtech.thecarcard.R;
import com.mtech.thecarcard.Server.MySingleton;
import com.mtech.thecarcard.Server.Server;
import com.mtech.thecarcard.utilities.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MemberShipDetailsActivity extends AppCompatActivity {
TextView ed_dateofbirth;
    private EditText ed_name, ed_email, ed_phonenumber, ed_password, ed_address;
    List<String> vehicle_year, vehicle_type, vehicle_model;
    private MaterialSpinner  sp_Vehicle_Model, sp_Vehicle_Year;
    MaterialSpinner sp_Vehicle_Make;
    private Button btn_Continue;
    private ImageView back_ic;
    String getdate;
    String vechile_type1, vehicle_year1, vehicle_model1, getVehicleType, getVehicleModel, getVehicleYear;
    ProgressDialog progressDialog;

    private DatePickerDialog.OnDateSetListener mDateSetListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_membershipdetails);

        ed_name = findViewById(R.id.ed_name);
        ed_email = findViewById(R.id.ed_email);
        ed_phonenumber = findViewById(R.id.ed_phonenumber);
        ed_password = findViewById(R.id.ed_password);
        ed_dateofbirth = findViewById(R.id.ed_dateofbirth);
        ed_address = findViewById(R.id.ed_address);
        sp_Vehicle_Make = findViewById(R.id.sp_vehicle_make);
        sp_Vehicle_Model = findViewById(R.id.sp_vehicle_model);
        sp_Vehicle_Year = findViewById(R.id.sp_vehicle_year);
        btn_Continue = findViewById(R.id.btn_signup);
        back_ic = findViewById(R.id.back_ic);
        getVehicleType();
        getyear();
        getVehicleModel();
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Creating Account..");


        back_ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(),BuyMemberShipActivity2.class);
                startActivity(intent);
                finish();

            }
        });

        btn_Continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progressDialog.show();
                String name = ed_name.getText().toString();
                String email = ed_email.getText().toString();
                String phone = ed_phonenumber.getText().toString();
                String password = ed_password.getText().toString();
//                String vehicleModel = sp_Vehicle_Model.getSelectedItem().toString();
//                String vehicleYear = sp_Vehicle_Year.getSelectedItem().toString();
                String packageId = String.valueOf(Utilities.getInt(getApplicationContext(), "packageId"));
                String coupon_code = Utilities.getString(getApplicationContext(), "coupon");
                String address = ed_address.getText().toString();
                if (name.isEmpty() || email.isEmpty() || phone.isEmpty() || password.isEmpty()) {
                    progressDialog.dismiss();
                    Toast.makeText(MemberShipDetailsActivity.this, "Please Add Missing Fields", Toast.LENGTH_SHORT).show();
                } else {
                    createUser(name, phone, email, password, getVehicleType, getVehicleModel, getVehicleYear, getdate, address, packageId,coupon_code);
                }

            }
        });
        ed_dateofbirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        MemberShipDetailsActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
//                Log.d(TAG, "onDateSet: mm/dd/yyy: " + month + "/" + day + "/" + year);
                getdate = day + "-" + month + "-" + year;
                ed_dateofbirth.setText(getdate);
            }
        };

    }

    private void createUser(String name, String phone, String email, String password, String vehicleName, String vehicleModel, String vehicleYear, String dob, String address, String packageId,String coupon_code) {

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);

        Call<LoginResponse> call = service.SignUpUser(name, phone, email, password, vehicleName, vehicleModel, vehicleYear, dob, address, packageId,coupon_code);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                progressDialog.dismiss();
                int status = response.body().getStatus();
                if (status == 200) {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    String userEmail = response.body().getData().getEmail();
                    String coupon = response.body().getData().getCoupon_code();
                    Intent mainIntent = new Intent(MemberShipDetailsActivity.this, PayActivity.class);

                    Utilities.saveString(getApplicationContext(), "email", userEmail);
                    Utilities.saveString(getApplicationContext(),"coupon_code",coupon);

                    startActivity(mainIntent);
                    finish();
                } else if (status == 400) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), response.body().getMessage() + "Failed", Toast.LENGTH_LONG).show();
                }

            }


            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), t.getMessage() + "Not Called", Toast.LENGTH_LONG).show();
            }
        });

    }

    private void getVehicleType() {


        final StringRequest RegistrationRequest = new StringRequest(Request.Method.GET, Server.vechile_record, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    ArrayList<VehicleTypeModel> vehicleTypeModels = new ArrayList<VehicleTypeModel>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    String message = object.getString("message");
                    if (status == 200) {
//                        Toast.makeText(MemberShipDetailsActivity.this, message, Toast.LENGTH_SHORT).show();
                        vehicle_type = new ArrayList<>();
                        JSONObject jsonObject;
                        JSONObject objUser = object.getJSONObject("data");
                        JSONArray vechile_type = objUser.getJSONArray("vechile_type");
                        for (int i = 0; i < vechile_type.length(); i++) {
                            jsonObject = vechile_type.getJSONObject(i);
                            vechile_type1 = jsonObject.getString("vechile_type");
                            vehicleTypeModels.add(new VehicleTypeModel(vechile_type1));
                            vehicle_type.add(vechile_type1);

                        }


                        // Creating adapter for spinner
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
                                MemberShipDetailsActivity.this, R.layout.spinner_item, vehicle_type
                        );

//                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(MemberShipDetailsActivity.this, android.R.layout.spinner_item, vehicle_type);
//                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        dataAdapter.setDropDownViewResource(R.layout.spinner_item);
                        sp_Vehicle_Make.setAdapter(dataAdapter);
//                        vehicle_type.add(0, "Vehicle Type");
//                        sp_Vehicle_Make.setSelection(0);
//                        sp_Vehicle_Make.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                            @Override
//                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                                getVehicleType = parent.getItemAtPosition(position).toString();
//                            }
//
//                            @Override
//                            public void onNothingSelected(AdapterView<?> parent) {
//
//                            }
//                        });
                        sp_Vehicle_Make.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                                getVehicleType = item.toString();

                            }
                        });

                    } else {
                        Toast.makeText(MemberShipDetailsActivity.this, message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (MemberShipDetailsActivity.this != null)
                    Toast.makeText(MemberShipDetailsActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                params.put("token", token);
                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(MemberShipDetailsActivity.this).addToRequestQueue(RegistrationRequest);


    }


    public void getyear() {


        final StringRequest RegistrationRequest = new StringRequest(Request.Method.GET, Server.vechile_record, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    ArrayList<VehicleYearModel> vehicleYearModels = new ArrayList<VehicleYearModel>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    String message = object.getString("message");
                    if (status == 200) {
//                        Toast.makeText(MemberShipDetailsActivity.this, message, Toast.LENGTH_SHORT).show();
                        vehicle_year = new ArrayList<>();
                        JSONObject jsonObject;
                        JSONObject objUser = object.getJSONObject("data");
                        JSONArray vechile_type = objUser.getJSONArray("vechile_year");
                        for (int i = 0; i < vechile_type.length(); i++) {
                            jsonObject = vechile_type.getJSONObject(i);
                            vehicle_year1 = jsonObject.getString("year");
                            vehicleYearModels.add(new VehicleYearModel(vehicle_year1));
                            vehicle_year.add(vehicle_year1);

                        }


                        // Creating adapter for spinner
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
                                MemberShipDetailsActivity.this, R.layout.spinner_item, vehicle_year
                        );

//                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(MemberShipDetailsActivity.this, android.R.layout.spinner_item, vehicle_type);
//                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        dataAdapter.setDropDownViewResource(R.layout.spinner_item);
                        sp_Vehicle_Year.setAdapter(dataAdapter);
//                        vehicle_year.add(0, "Vehicle Year");
//                        sp_Vehicle_Year.setSelection(0);
                        sp_Vehicle_Year.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                                getVehicleYear =item.toString();

                            }
                        });

                    } else {
                        Toast.makeText(MemberShipDetailsActivity.this, message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (MemberShipDetailsActivity.this != null)
                    Toast.makeText(MemberShipDetailsActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                params.put("token", token);
                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(MemberShipDetailsActivity.this).addToRequestQueue(RegistrationRequest);


    }


    private void getVehicleModel() {


        final StringRequest RegistrationRequest = new StringRequest(Request.Method.GET, Server.vechile_record, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    ArrayList<ModelVehicleModel> modelVehicleModels = new ArrayList<ModelVehicleModel>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    String message = object.getString("message");
                    if (status == 200) {
//                        Toast.makeText(MemberShipDetailsActivity.this, message, Toast.LENGTH_SHORT).show();
                        vehicle_model = new ArrayList<>();
                        JSONObject jsonObject;
                        JSONObject objUser = object.getJSONObject("data");
                        JSONArray vechile_type = objUser.getJSONArray("vechile_model");
                        for (int i = 0; i < vechile_type.length(); i++) {
                            jsonObject = vechile_type.getJSONObject(i);
                            vehicle_model1 = jsonObject.getString("vechile_model");
                            modelVehicleModels.add(new ModelVehicleModel(vehicle_model1));
                            vehicle_model.add(vehicle_model1);

                        }


                        // Creating adapter for spinner
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
                                MemberShipDetailsActivity.this, R.layout.spinner_item, vehicle_model
                        );

//                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(MemberShipDetailsActivity.this, android.R.layout.spinner_item, vehicle_type);
//                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        dataAdapter.setDropDownViewResource(R.layout.spinner_item);
                        sp_Vehicle_Model.setAdapter(dataAdapter);
//                        vehicle_model.add(0, "Vehicle Model");
//                        sp_Vehicle_Model.setSelection(0);
                        sp_Vehicle_Model.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                                getVehicleModel=item.toString();
                            }
                        });

                    } else {
                        Toast.makeText(MemberShipDetailsActivity.this, message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (MemberShipDetailsActivity.this != null)
                    Toast.makeText(MemberShipDetailsActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                params.put("token", token);
                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(MemberShipDetailsActivity.this).addToRequestQueue(RegistrationRequest);


    }
}

