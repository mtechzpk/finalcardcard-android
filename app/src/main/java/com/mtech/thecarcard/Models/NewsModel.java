package com.mtech.thecarcard.Models;

public class NewsModel {


    private  Integer image;
    private  String title,time,announcement;

    public NewsModel(Integer image, String title, String time, String announcement) {
        this.image = image;
        this.title = title;
        this.time = time;
        this.announcement = announcement;
    }

    public NewsModel() {
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(String announcement) {
        this.announcement = announcement;
    }
}
