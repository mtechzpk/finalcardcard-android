package com.mtech.thecarcard.Models;

import com.google.gson.annotations.SerializedName;

public class OfferDataModel {

    @SerializedName("id")
    private int id;
    @SerializedName("post_title")
    private String post_title;
    @SerializedName("sub_title")
    private String sub_title;
    @SerializedName("category_id")
    private int category_id;
    @SerializedName("description")
    private String description;
    @SerializedName("image")
    private String image;
    @SerializedName("user_id")
    private int user_id;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("category_name")
    private String category_name;


    public OfferDataModel(int id, String post_title, String sub_title, int category_id, String description, String image, int user_id, String created_at, String category_name) {
        this.id = id;
        this.post_title = post_title;
        this.sub_title = sub_title;
        this.category_id = category_id;
        this.description = description;
        this.image = image;
        this.user_id = user_id;
        this.created_at = created_at;
        this.category_name = category_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPost_title() {
        return post_title;
    }

    public void setPost_title(String post_title) {
        this.post_title = post_title;
    }

    public String getSub_title() {
        return sub_title;
    }

    public void setSub_title(String sub_title) {
        this.sub_title = sub_title;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }
}
