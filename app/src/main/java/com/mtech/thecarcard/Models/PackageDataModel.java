package com.mtech.thecarcard.Models;

import com.google.gson.annotations.SerializedName;

public class PackageDataModel {
    @SerializedName("id")
    private int id;
    @SerializedName("amount")
    private String amount;
    @SerializedName("duration")
    private String duration;
    @SerializedName("description")
    private String description;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("updated_at")
    private String updated_at;
    @SerializedName("coupon_code")
    private String coupon_code;
    @SerializedName("discount")
    private String discount;
    @SerializedName("discount_amount")
    private String discount_amount;

    public PackageDataModel(int id, String amount, String duration, String description, String created_at, String updated_at, String coupon_code, String discount, String discount_amount) {
        this.id = id;
        this.amount = amount;
        this.duration = duration;
        this.description = description;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.coupon_code = coupon_code;
        this.discount = discount;
        this.discount_amount = discount_amount;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscount_amount() {
        return discount_amount;
    }

    public void setDiscount_amount(String discount_amount) {
        this.discount_amount = discount_amount;
    }

    public String getCoupon_code() {
        return coupon_code;
    }

    public void setCoupon_code(String coupon_code) {
        this.coupon_code = coupon_code;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
