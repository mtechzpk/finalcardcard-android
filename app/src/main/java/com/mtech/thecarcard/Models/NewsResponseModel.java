package com.mtech.thecarcard.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NewsResponseModel {
    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<NewsDataModel> data;


    public NewsResponseModel(int status, String message, List<NewsDataModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<NewsDataModel> getData() {
        return data;
    }

    public void setData(List<NewsDataModel> data) {
        this.data = data;
    }
}