package com.mtech.thecarcard.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OfferResponseModel {

    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<OfferDataModel> data;

    public OfferResponseModel(int status, String message, List<OfferDataModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<OfferDataModel> getData() {
        return data;
    }

    public void setData(List<OfferDataModel> data) {
        this.data = data;
    }
}
