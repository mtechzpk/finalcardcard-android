package com.mtech.thecarcard.Models;

public class VehicleTypeModel {
    int id = 0;
    String vehicle_type = "";

    public VehicleTypeModel(String vehicle_type) {
        this.vehicle_type = vehicle_type;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setArea_type(String area_type) {
        this.vehicle_type = area_type;
    }
}
