package com.mtech.thecarcard.Models;

public class ModelVehicleModel {
    int id = 0;
    String vehicle_model = "";

    public ModelVehicleModel(String vehicle_model) {
        this.vehicle_model = vehicle_model;
    }

    public String getVehicle_model() {
        return vehicle_model;
    }

    public void setVehicle_model(String vehicle_model) {
        this.vehicle_model = vehicle_model;
    }
}
