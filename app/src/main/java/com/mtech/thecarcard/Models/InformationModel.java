package com.mtech.thecarcard.Models;

public class InformationModel {


    private  Integer image;
    private  String title,question;

    public InformationModel(Integer image, String title, String question) {
        this.image = image;
        this.title = title;
        this.question = question;
    }

    public InformationModel() {
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }
}
