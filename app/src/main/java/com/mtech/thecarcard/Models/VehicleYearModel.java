package com.mtech.thecarcard.Models;

public class VehicleYearModel {
    int id = 0;
    String vehicle_year = "";

    public VehicleYearModel(String vehicle_year) {
        this.vehicle_year = vehicle_year;
    }

    public String getVehicle_year() {
        return vehicle_year;
    }

    public void setVehicle_year(String vehicle_year) {
        this.vehicle_year = vehicle_year;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
