package com.mtech.thecarcard.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PackageResponseModel {

    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<PackageDataModel> data;

    public PackageResponseModel(int status, String message, List<PackageDataModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<PackageDataModel> getData() {
        return data;
    }

    public void setData(List<PackageDataModel> data) {
        this.data = data;
    }
}
