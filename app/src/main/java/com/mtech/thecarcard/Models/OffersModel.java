package com.mtech.thecarcard.Models;

public class OffersModel {


    private  Integer image;
    private  String store,date,offers;

    public OffersModel(Integer image, String store, String date, String offers) {
        this.image = image;
        this.store = store;
        this.date = date;
        this.offers = offers;
    }

    public OffersModel() {
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOffers() {
        return offers;
    }

    public void setOffers(String offers) {
        this.offers = offers;
    }
}
