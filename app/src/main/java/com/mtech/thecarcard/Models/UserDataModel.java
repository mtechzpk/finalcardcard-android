package com.mtech.thecarcard.Models;

import com.google.gson.annotations.SerializedName;

public class UserDataModel {

    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("email")
    private String email;
    @SerializedName("phone")
    private String phone;
    @SerializedName("vechle_name")
    private String vechle_name;
    @SerializedName("vechle_model")
    private String vechle_model;
    @SerializedName("vechle_year")
    private String vechle_year;
    @SerializedName("dateofbirth")
    private String dateofbirth;
    @SerializedName("address")
    private String address;
    @SerializedName("pakage_id")
    private String pakage_id;
    @SerializedName("amount")
    private String amount;
    @SerializedName("membership_token")
    private String membership_token;
    @SerializedName("expire_date")
    private String expire_date;
 @SerializedName("coupon_code")
    private String coupon_code;


    public UserDataModel(int id, String name, String email, String phone, String vechle_name, String vechle_model, String vechle_year, String dateofbirth, String address, String pakage_id, String amount, String membership_token, String expire_date, String coupon_code) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.vechle_name = vechle_name;
        this.vechle_model = vechle_model;
        this.vechle_year = vechle_year;
        this.dateofbirth = dateofbirth;
        this.address = address;
        this.pakage_id = pakage_id;
        this.amount = amount;
        this.membership_token = membership_token;
        this.expire_date = expire_date;
        this.coupon_code = coupon_code;
    }

    public String getCoupon_code() {
        return coupon_code;
    }

    public void setCoupon_code(String coupon_code) {
        this.coupon_code = coupon_code;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getVechle_name() {
        return vechle_name;
    }

    public void setVechle_name(String vechle_name) {
        this.vechle_name = vechle_name;
    }

    public String getVechle_model() {
        return vechle_model;
    }

    public void setVechle_model(String vechle_model) {
        this.vechle_model = vechle_model;
    }

    public String getVechle_year() {
        return vechle_year;
    }

    public void setVechle_year(String vechle_year) {
        this.vechle_year = vechle_year;
    }

    public String getDateofbirth() {
        return dateofbirth;
    }

    public void setDateofbirth(String dateofbirth) {
        this.dateofbirth = dateofbirth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPakage_id() {
        return pakage_id;
    }

    public void setPakage_id(String pakage_id) {
        this.pakage_id = pakage_id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMembership_token() {
        return membership_token;
    }

    public void setMembership_token(String membership_token) {
        this.membership_token = membership_token;
    }

    public String getExpire_date() {
        return expire_date;
    }

    public void setExpire_date(String expire_date) {
        this.expire_date = expire_date;
    }
}